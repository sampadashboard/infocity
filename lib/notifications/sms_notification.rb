# frozen_string_literal: true

require_relative "composite_notification"
require "twilio-ruby"

class SMSNotification < CompositeNotification
  def initialize(number)
    @number = number
    super()
  end

  def send
    account_sid = ENV["TWILIO_ACC_SID"]
    auth_token = ENV["TWILIO_AUTH_TOKEN"]
    sender_numer = ENV["TWILIO_NUMBER"]
    @client = Twilio::REST::Client.new account_sid, auth_token

    message = @client.messages.create(
      body: "Notificação de alarme",
      to: @number,
      from: sender_numer
    )

    super
  end
end
