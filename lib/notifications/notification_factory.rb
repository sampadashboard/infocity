# frozen_string_literal: true

require_relative "mail_notification"
require_relative "sms_notification"
require_relative "webhook_notification"

class NotificationFactory
  # Use getShape method to get object of type shape.
  def get_notification(alarm)
    comp = CompositeNotification.new

    user_data = get_user_data alarm.user_id if alarm.sms || alarm.email

    if alarm.email
      mail = MailNotification.new user_data["email"]
      comp << mail
    end

    if alarm.sms
      user_data["phone"] = "+5511994013131"
      sms = SMSNotification.new user_data["phone"]
      comp << sms
    end

    if alarm.webhook && (alarm.webhook != "")
      web = WebhookNotification.new "https://requestb.in/19tccxn1"
      comp << web
    end

    comp
  end

  def get_user_data(token)
    url = "http://sampadashboard.herokuapp.com/users?user_token=" + token
    uri = URI.parse(url)

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true if uri.scheme == "https"
    request = Net::HTTP::Get.new(uri.request_uri)

    response = http.request(request)
    response = JSON.parse response.body
  end
end
