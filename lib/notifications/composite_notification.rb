# frozen_string_literal: true

require_relative "notification"

class CompositeNotification < Notification
  attr_accessor :parent

  def initialize
    @notifications = []
  end

  def add_notification(notification)
    @notifications << notification
    notification.parent = self
  end

  alias << add_notification

  def remove_notification(notification)
    @notifications.delete(notification)
    notification.parent = nil
  end

  def [](index)
    @notifications[index]
  end

  def []=(index, value)
    replaced_value = @notifications[index]
    @notifications[index] = value
    replaced_value.parent = nil
    value.parent = self
  end

  def send
    @notifications.each(&:send)
  end
end
