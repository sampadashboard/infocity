# frozen_string_literal: true

require_relative "composite_notification"
require_relative "../../app/mailers/sender_mailer"

class MailNotification < CompositeNotification
  def initialize(mail)
    @mail = mail
    super()
  end

  def send
    mailer = SenderMailer
    mailer.send_email(@mail).deliver_now
    super
  end
end
