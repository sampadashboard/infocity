# frozen_string_literal: true

class Notification
  def send
    raise "must implement send() method in subclass"
  end
end
