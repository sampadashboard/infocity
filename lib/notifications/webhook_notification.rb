# frozen_string_literal: true

require_relative "composite_notification"

class WebhookNotification < CompositeNotification
  def initialize(url)
    @url = url
    super()
  end

  def send
    uri = URI.parse(@url)
    header = { "Content-Type" => "application/json" }

    payload = {
      "alarm" => true
    }

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true if uri.scheme == "https"
    request = Net::HTTP::Post.new(uri.request_uri, header)
    request.body = payload.to_json

    response = http.request(request)
    super
  end
end
