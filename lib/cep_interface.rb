# frozen_string_literal: true

require "net/http"
require "uri"
require "json"
require "time"

class CepInterface
  def initialize(endpoint)
    @uri = URI.parse(endpoint)
  end

  def send_events(events)
    puts("CepInterface: starting processing events")

    events["resources"].each do |resource|
      resource["capabilities"].each do |capability|
        events = capability[1]
        events.each do |event|
          event = {
            "uuid" => resource["uuid"],
            "lat" => resource["lat"],
            "lon" => resource["lon"],
            "capability" => capability[0],
            "data" => event
          }

          send_event(event)
        end
      end
    end
    puts("CepInterface: finished processing events")
  end

  def send_event(event)
    request = Net::HTTP::Post.new(@uri.to_s, @header)
    # puts event.to_json
    request.body = event.to_json

    # Send the request
    Net::HTTP.start(@uri.host, @uri.port, use_ssl: @uri.scheme == "https") do |http|
      res = http.request(request)
      if res.code != "200"
        puts "Fail to insert data into CEP! Response from CEP: " + res.code
        puts res.body
      else
        puts "CepInterface: Event sent to CEP"
      end
    end
  end
end
