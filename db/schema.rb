# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171130100105) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "alarms", force: :cascade do |t|
    t.string "user_id"
    t.boolean "email"
    t.boolean "sms"
    t.string "webhook"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.float "latitude"
    t.float "longitude"
    t.boolean "actived"
    t.string "flow_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "capability"
    t.string "comparator"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "alarm_id"
    t.index ["alarm_id"], name: "index_events_on_alarm_id"
  end

  create_table "flows", force: :cascade do |t|
    t.string "nr_id", null: false
    t.boolean "status", null: false
    t.string "name"
    t.integer "alarm_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "link_node"
  end

  create_table "linked_flows", force: :cascade do |t|
    t.integer "flow_in_id"
    t.integer "flow_out_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "events", "alarms"
  add_foreign_key "flows", "alarms"
  add_foreign_key "linked_flows", "flows", column: "flow_in_id"
  add_foreign_key "linked_flows", "flows", column: "flow_out_id"
end
