class AddFlowIdToAlarm < ActiveRecord::Migration[5.1]
  def change
    add_column :alarms, :flow_id, :string
  end
end
