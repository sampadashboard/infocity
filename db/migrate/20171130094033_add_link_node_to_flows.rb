class AddLinkNodeToFlows < ActiveRecord::Migration[5.1]
  def change
    add_column :flows, :link_node, :string
  end
end
