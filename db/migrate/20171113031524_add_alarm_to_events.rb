class AddAlarmToEvents < ActiveRecord::Migration[5.1]
  def change
    add_reference :events, :alarm, foreign_key: true
  end
end
