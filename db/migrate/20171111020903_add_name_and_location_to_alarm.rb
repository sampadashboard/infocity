class AddNameAndLocationToAlarm < ActiveRecord::Migration[5.1]
  def change
    add_column :alarms, :name, :string
    add_column :alarms, :lat, :float
    add_column :alarms, :lon, :float
  end
end
