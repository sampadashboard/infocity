class ChangeColumnName < ActiveRecord::Migration[5.1]
  def change
  	rename_column :alarms, :push, :sms
  end
end
