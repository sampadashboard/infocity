class FixAlarmsFields < ActiveRecord::Migration[5.1]
  def change
    rename_column :alarms, :lat, :latitude
    rename_column :alarms, :lon, :longitude
    change_column :alarms, :webhook, :string
  end
end
