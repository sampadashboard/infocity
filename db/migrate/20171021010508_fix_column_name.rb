class FixColumnName < ActiveRecord::Migration[5.1]
  def change
    rename_column :alarms, :query, :query_id
    rename_column :alarms, :user, :user_id
  end
end
