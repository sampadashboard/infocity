class AddActivedToAlarm < ActiveRecord::Migration[5.1]
  def change
    add_column :alarms, :actived, :boolean
  end
end
