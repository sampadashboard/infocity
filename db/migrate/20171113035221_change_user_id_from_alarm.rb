class ChangeUserIdFromAlarm < ActiveRecord::Migration[5.1]
  def change
    change_column :alarms, :user_id, :string
  end
end
