class RemoveQueryIdFromAlarm < ActiveRecord::Migration[5.1]
  def change
    remove_column :alarms, :query_id, :integer
  end
end
