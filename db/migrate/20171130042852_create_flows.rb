class CreateFlows < ActiveRecord::Migration[5.1]
  def change
    create_table :flows do |t|
      t.string :nr_id, null: false
      t.boolean :status, null: false
      t.string :name
      t.integer :alarm_id

      t.timestamps
    end
    add_foreign_key :flows, :alarms
  end
end
