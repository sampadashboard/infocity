class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :capability
      t.string :comparator
      t.string :value

      t.timestamps
    end
  end
end
