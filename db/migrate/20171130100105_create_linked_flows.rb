class CreateLinkedFlows < ActiveRecord::Migration[5.1]
  def change
    create_table :linked_flows do |t|
      t.integer :flow_in_id
      t.integer :flow_out_id

      t.timestamps
    end
    add_foreign_key :linked_flows, :flows, column: :flow_in_id
    add_foreign_key :linked_flows, :flows, column: :flow_out_id
  end
end
