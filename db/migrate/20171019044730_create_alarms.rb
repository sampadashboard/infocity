class CreateAlarms < ActiveRecord::Migration[5.1]
  def change
    create_table :alarms do |t|
      t.integer :user
      t.boolean :email
      t.boolean :push
      t.boolean :webhook
      t.integer :query

      t.timestamps
    end
  end
end
