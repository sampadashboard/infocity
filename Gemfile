# frozen_string_literal: true

source "https://rubygems.org"

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

ruby "2.4.1"
gem "ast", "~> 2.3.0"
gem "bcrypt", "~> 3.1.7"
gem "coffee-rails", "~> 4.2"
gem "figaro"
gem "jbuilder", "~> 2.5"
gem "pg"
gem "pry"
gem "pry-byebug"
gem "puma", "~> 3.7"
gem "rails", "~> 5.1.4"
gem "redis", "~> 3.0"
gem "sass-rails", "~> 5.0"
gem "turbolinks", "~> 5"
gem "twilio-ruby", "~> 5.4.5"
gem "uglifier", ">= 1.3.0"
# Use Capistrano for deployment
# gem "capistrano-rails", group: :development

group :development, :test do
  gem "byebug", platforms: %i[mri mingw x64_mingw]
  gem "capybara", "~> 2.13"
  gem "rspec-rails", "~> 3.6"
  gem "selenium-webdriver"
end

group :development do
  gem "listen", ">= 3.0.5", "< 3.2"
  gem "rubocop"
  gem "spring"
  gem "spring-watcher-listen", "~> 2.0.0"
  gem "web-console", ">= 3.3.0"
end

gem "tzinfo"
gem "tzinfo-data"

# Added at 2017-10-08 20:11:19 -0300 by diego:
gem "clockwork"
