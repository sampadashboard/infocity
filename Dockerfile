FROM ruby:2.4.1-alpine

ENV BUILD_PACKAGES="linux-headers \
                    curl \
                    curl-dev \
                    build-base \
                    openssh \
                    git \
                    libffi-dev \
                    bash \
                    jq \
                    postgresql-dev \
                    nodejs \
                    libxslt-dev \
                    less \
                    libxml2-dev"

RUN apk --update --upgrade add $BUILD_PACKAGES \
    && rm /var/cache/apk/*

WORKDIR /infocity

COPY Gemfile* /infocity/

RUN bundle install --jobs=4

COPY . /infocity/

EXPOSE 3000

CMD ["bundle", "exec", "rails", "server"]
