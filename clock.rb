# frozen_string_literal: true

require "clockwork"
require_relative "app/workers/event_reader_worker.rb"

module Clockwork
  read_event_interval = ENV["READ_EVENT_INTERVAL"] ? ENV["READ_EVENT_INTERVAL"].to_i : 60
  intercity_endpoint = ENV["INTERCITY_ENDPOINT"] ? ENV["INTERCITY_ENDPOINT"] : "http://35.198.3.112"

  worker = EventReaderWorker.new(intercity_endpoint)

  handler do |_job, time|
    # Worker parameters
    start_time = time - read_event_interval
    end_time = time

    worker.work(start_time, end_time)
  end

  every(read_event_interval.seconds, "EventReaderWorker")
end
