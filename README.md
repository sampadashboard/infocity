[![pipeline status](https://gitlab.com/sampadashboard/infocity/badges/master/pipeline.svg)](https://gitlab.com/sampadashboard/infocity/commits/master)

InfoCity
========

InfoCity é um sistema de notificação de eventos complexos. Através de uma API, o cliente pode registrar os critérios desejados para o disparo da notificação. Os critérios serão provenientes dos diversos sensores instalados na cidade ou informações de data e hora. O sistema disponibilizará diversos meios para envio de notificação, como, por exemplo, envio de SMS, e-mail e integração com webhooks.

Build:
------

O projeto é feito em rails 5 e apresenta uma imagem em docker, logo é bem simples subir via docker

```bash
docker-compose up --build
docker-compose exec infocity rake db:create
docker-compose exec infocity rake db:migrate
```
Testes:
------
Rode os testes dentro do container docker infocity. Após subir a aplicação execute:

```bash
docker-compose exec infocity bundle exec rspec
```


Na porta `3000` estará a aplicação e o banco de dados (postgresql) na porta `5000`


Principais requisitos:
---------------------
 - Gerenciamento das notificações, onde o usuário pode cadastrar eventos complexos para os quais queira ser alertado.
 - Análise das queries cadastradas, coletando e analisando os dados dos sistemas existentes para que sejam disparadas as notificações.
 - Envio de notificações via SMS, email e webhook, para que o usuário seja alertado sobre o evento que cadastrou, e que seja mostrado no dashboard a informação solicitada.
