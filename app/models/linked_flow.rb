# frozen_string_literal: true

class LinkedFlow < ApplicationRecord
  belongs_to :flow_in, class_name: "Flow"
  belongs_to :flow_out, class_name: "Flow"
end
