# frozen_string_literal: true

class Alarm < ApplicationRecord
  has_many :events
  has_many :flows

  accepts_nested_attributes_for :events, allow_destroy: true

  def add_flows(params)
    capabilities = params.to_h[:events_attributes]
    matchers = []

    capabilities.each do |filter|
      matchers << {
        capability: filter[:capability],
        comparator: map_comparator(filter[:comparator]),
        value:      filter[:value]
      }
    end
    matchers.each { |match| add_flow match }
  end

  def map_comparator(comparator)
    return "lt" if comparator == "<"
    return "gt" if comparator == ">"
    return "eq" if comparator == "="
  end

  def add_flow(matcher)
    flow = Flow.build_matcher(self, matcher)
    flow.save
    Flow.find_by_name(matcher[:capability]).flow_linkage flow
    flows << flow
  end

  def actived?
    flows.map(&:status).all?
  end
end
