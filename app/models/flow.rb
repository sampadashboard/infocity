# frozen_string_literal: true

class Flow < ApplicationRecord
  belongs_to :alarm

  has_many :linked_outer_flows, foreign_key: :flow_in_id, class_name: "LinkedFlow"
  has_many :linked_inner_flows, foreign_key: :flow_out_id, class_name: "LinkedFlow"

  has_many :flow_out, through: :linked_outer_flows
  has_many :flow_in, through: :linked_inner_flows

  validates :nr_id, :name, presence: true

  URL  = URI.parse "http://#{Rails.configuration.node_red_url}:1880/flow"
  HTTP = Net::HTTP.new URL.host, URL.port
  HEADER = { "Content-Type" => "application/json" }.freeze

  def self.build_matcher(alarm, matcher)
    flow = new
    flow.name   = rand(1..10_000).to_s
    flow.nr_id  = flow.create_nr_matcher(flow.name, matcher)
    flow.link_node = "#{flow.name}link"
    flow.alarm  = alarm
    flow.status = false
    flow
  end

  def self.build_receiver(alarm, entry_point, linked_flows)
    flow = new
    flow.name      = entry_point
    flow.link_node = "#{flow.name}link"
    flow.alarm     = alarm
    flow.status    = false

    # flow.flow_linkage linked_flows

    links = linked_flows.map(&:link_node)

    flow.nr_id = flow.create_nr_receiver(flow.name, entry_point, links)
    flow
  end

  def flow_linkage(linked_flow)
    flow_out << linked_flow
    update_linker(linked_flow.link_node)
    linked_flow.update_linker(link_node)
  end

  def update_linker(link)
    json_flow = bring_flow
    link_in_node = json_flow["nodes"].select { |x| x["type"] == "link in" }.last
    link_out_node = json_flow["nodes"].select { |x| x["type"] == "link out" }.last

    if link_in_node
      link_in_node["links"] << link
      link_in_node["links"].uniq!
    elsif link_out_node
      link_out_node["links"] << link
      link_out_node["links"].uniq!
    end

    uri = URI.parse "http://#{Rails.configuration.node_red_url}:1880/flow"
    request = Net::HTTP::Put.new("#{uri.request_uri}/#{nr_id}", HEADER)
    request.body = json_flow.to_json
    resp = HTTP.request(request)
    raise "Could not update nr flow #{resp.body}" if resp.code.to_i != 200
  end

  def bring_flow
    uri = URI.parse "http://#{Rails.configuration.node_red_url}:1880/flow/#{nr_id}"
    request = Net::HTTP::Get.new(uri.request_uri, HEADER)
    response = HTTP.request(request)
    JSON.parse response.body
  end

  def create_nr_receiver(name, entry_point, links)
    ids = {
      http_in: rand(1..1000).to_s,
      http_response: rand(1..1000).to_s,
      template: rand(1..1000).to_s,
      link_out:  "#{name}link"
    }
    payload = {
      "label": name,
      "nodes": [
        {
          "id": ids[:http_in],
          "type": "http in",
          "z": "7ce7ffdb.a31aa8",
          "name": "",
          "url": "/#{entry_point}",
          "method": "post",
          "upload": false,
          "swaggerDoc": "",
          "x": 80,
          "y": 100,
          "wires": [
            [
              ids[:template],
              ids[:link_out]
            ]
          ]
        },
        {
          "id": ids[:http_response],
          "type": "http response",
          "z": "7ce7ffdb.a31aa8",
          "name": "ok",
          "statusCode": "200",
          "headers": {},
          "x": 410,
          "y": 40,
          "wires": []
        },
        {
          "id": ids[:template],
          "type": "template",
          "z": "7ce7ffdb.a31aa8",
          "name": "response",
          "field": "payload",
          "fieldType": "msg",
          "format": "handlebars",
          "syntax": "plain",
          "template": "",
          "output": "str",
          "x": 240,
          "y": 40,
          "wires": [
            [
              ids[:http_response]
            ]
          ]
        },
        {
          "id": ids[:link_out],
          "type": "link out",
          "z": "7ce7ffdb.a31aa8",
          "name": "593",
          "links": links,
          "x": 455,
          "y": 100,
          "wires": []
        }
      ]
    }

    request = Net::HTTP::Post.new(URL.request_uri, HEADER)
    request.body = payload.to_json

    response = HTTP.request(request)
    response = JSON.parse response.body

    response["id"]
  end

  def create_nr_matcher(name, matcher, links = [])
    return nr_id if nr_id

    ids = {
      switch: rand(1..1000).to_s,
      http_request: rand(1..1000).to_s,
      template: rand(1..1000).to_s,
      link_in: "#{name}link"

    }
    payload = {
      "label" => name,
      "nodes" => [
        {
          "id": ids[:template],
          "type": "template",
          "z": "5bc4a958.929dd8",
          "name": "fact",
          "field": "payload",
          "fieldType": "msg",
          "format": "json",
          "syntax": "plain",
          "template": "{ \"result\": true}",
          "output": "json",
          "x": 410,
          "y": 120,
          "wires": [
            [
              ids[:http_request]
            ]
          ]
        },
        {
          "id": ids[:switch],
          "type": "switch",
          "z": "5bc4a958.929dd8",
          "name": matcher[:capability],
          "property": "payload.data.#{matcher[:capability]}",
          "propertyType": "msg",
          "rules": [
            {
              "t": matcher[:comparator],
              "v": matcher[:value].to_s,
              "vt": (matcher[:value].to_i.zero? ? "str" : "num")
            }
          ],
          "checkall": "true",
          "outputs": 1,
          "x": 200,
          "y": 120,
          "wires": [
            [
              ids[:template]
            ]
          ]
        },
        {
          "id": ids[:http_request],
          "type": "http request",
          "z": "5bc4a958.929dd8",
          "name": "query success",
          "method": "POST",
          "ret": "txt",
          "url": "http://#{Rails.configuration.infocity_url}:3000/#{name}/match",
          "tls": "",
          "x": 600,
          "y": 120,
          "wires": [
            []
          ]
        },
        {
          "id": ids[:link_in],
          "type": "link in",
          "z": "5bc4a958.929dd8",
          "name": "",
          "links": links,
          "x": 55,
          "y": 120,
          "wires": [
            [
              ids[:switch]
            ]
          ]
        }
      ]
    }

    request = Net::HTTP::Post.new(URL.request_uri, HEADER)
    request.body = payload.to_json

    response = HTTP.request(request)
    response = JSON.parse response.body

    response["id"]
  end

  def destroy_nr_flow
    return false unless nr_id
    destroy_nr
    update_attribute(:nr_id, "0")
  end

  def destroy_nr
    request = Net::HTTP::Delete.new "http://#{URL.request_uri}/#{nr_id}"
    resp = HTTP.request request
    raise "Could not destroy nr flow #{resp.inspect}" if resp.code.to_i != 204
  end
end
