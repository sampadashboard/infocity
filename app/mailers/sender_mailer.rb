# frozen_string_literal: true

class SenderMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.sender_mailer.send_email.subject
  #
  def send_email(email)
    mail to: email, subject: "Notificação de alarme"
  end
end
