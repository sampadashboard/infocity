# frozen_string_literal: true

require "net/http"
require "uri"
require "json"
require "time"
require_relative "../../config/environment"
require_relative "../../lib/cep_interface.rb"

class EventReaderWorker
  def initialize(endpoint)
    @endpoint = endpoint
    @data_uri = URI.parse(endpoint + "/collector/resources/data")
    @header = { 'Content-Type': "application/json" }
    @http = Net::HTTP.new(@data_uri.host, @data_uri.port)
  end

  def work(start_time, end_time)
    puts "EventReaderWorker: Running from #{start_time} to #{end_time}"

    body = {
      start_date: start_time.iso8601,
      end_date: end_time.iso8601
    }

    request = Net::HTTP::Post.new(@data_uri.request_uri, @header)
    request.body = body.to_json

    # Send the request
    puts "EventReaderWorker: Requesting events"
    response = @http.request(request)
    events = JSON.parse(response.body)

    puts "EventReaderWorker: Processing #{events["resources"].length} events"

    for event in events["resources"] do
      puts "EventReaderWorker: Getting sensor info uuid: #{event["uuid"]}"

      @sensor_uri = URI.parse(@endpoint + "/catalog/resources/" + event["uuid"])
      sensor_request = Net::HTTP::Get.new(@sensor_uri, @header)
      sensor_response = @http.request(sensor_request)

      if sensor_response.code == "200"
        sensor = JSON.parse(sensor_response.body)
        event["lat"] = sensor["data"]["lat"]
        event["lon"] = sensor["data"]["lon"]
      else
        puts("EventReaderWorker: Error getting sensor data. Http code: #{sensor_response.code}")
      end
    end

    puts("EventReaderWorker: finished gathering data")

    # Cannot use rails here
    # puts Rails.configuration.node_red_url
    node_red_url = Rails.configuration.node_red_url

    # TODO
    # Use real CEP url
    cep = CepInterface.new(node_red_url)

    cep.send_events(events)
  end
end
