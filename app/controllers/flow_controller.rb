# frozen_string_literal: true

class FlowController < ApplicationController
  def new_master
    resource = params[:var]

    master_alarm = Alarm.find_or_create_by(name: "master alarm")

    if Flow.where(name: resource).empty?
      flash.notice = "Flow mestre criado com sucesso"
      flow = Flow.build_receiver master_alarm, resource, []
      flow.save!
    else
      flash.notice = "Flow mestre já existe"
    end

    render "/welcome/index"
  end
end
