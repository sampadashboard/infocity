# frozen_string_literal: true

require_relative "../../lib/notifications/notification_factory.rb"

class AlarmsController < ApplicationController
  before_action :set_alarm, only: %i[show edit update destroy match]
  protect_from_forgery unless: -> { request.format.json? }

  # GET /alarms
  # GET /alarms.json
  def index
    @alarms = Alarm.all
  end

  # GET /alarms/1
  # GET /alarms/1.json
  def show; end

  # GET /alarms/new
  def new
    @alarm = Alarm.new
    2.times { @alarm.events.build }
  end

  # GET /alarms/1/edit
  def edit; end

  # POST /alarms
  # POST /alarms.json
  def create
    @alarm = Alarm.new(alarm_params)

    respond_to do |format|
      if @alarm.save
        @alarm.add_flows(alarm_params)
        format.html { redirect_to @alarm, notice: "Alarm was successfully created." }
        format.json { render :show, status: :created, location: @alarm }
      else
        format.html { render :new }
        format.json { render json: @alarm.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /alarms/1
  # PATCH/PUT /alarms/1.json
  def update
    respond_to do |format|
      if @alarm.update(alarm_params)
        format.html { redirect_to @alarm, notice: "Alarm was successfully updated." }
        format.json { render :show, status: :ok, location: @alarm }
      else
        format.html { render :edit }
        format.json { render json: @alarm.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /alarms/1
  # DELETE /alarms/1.json
  def destroy
    @alarm.destroy
    respond_to do |format|
      format.html { redirect_to alarms_url, notice: "Alarm was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def match
    result = params[:result]
    if result
      if @alarm.actived?
        comp = NotificationFactory.new
        notification = comp.get_notification(@alarm)
        notification.send
        @alarm.actived = true
        @alarm.save
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_alarm
    @alarm = Alarm.find(params[:id])
    return unless @alarm

    # flow = Flow.find_by(name: params[:id]).first
    # flow.update_attributes(status: true)
    # @alarm = flow.alarm
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def alarm_params
    params.require(:alarm).permit(
      :user_id, :name, :actived, :latitude, :longitude, :email, :sms, :webhook, :flow_id,
      events_attributes: %i[capability comparator value _destroy]
    )
  end
end
