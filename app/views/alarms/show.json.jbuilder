# frozen_string_literal: true

json.partial! "alarms/alarm", alarm: @alarm
