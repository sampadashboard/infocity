# frozen_string_literal: true

json.extract! alarm,
  :id, :user_id, :name, :latitude, :longitude, :email, :sms, :webhook, :actived,
  :events
json.url alarm_url(alarm, format: :json)
