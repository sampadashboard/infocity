# frozen_string_literal: true

json.array! @alarms, partial: "alarms/alarm", as: :alarm
