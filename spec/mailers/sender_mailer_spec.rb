# frozen_string_literal: true

require "rails_helper"

RSpec.describe SenderMailer, type: :mailer do
  describe "send_email" do
    email = "to@example.org"
    let(:mail) { SenderMailer.send_email email }

    it "renders the subject" do
      expect(mail.subject).to eq("Notificação de alarme")
    end

    it "renders the receiver email" do
      expect(mail.to).to eq([email])
    end

    it "renders the sender email" do
      expect(mail.from).to eq(["from@example.com"])
    end
  end
end
