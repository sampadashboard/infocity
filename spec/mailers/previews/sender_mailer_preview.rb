# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/sender_mailer
class SenderMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/sender_mailer/send_email
  def send_email
    SenderMailerMailer.send_email
  end
end
