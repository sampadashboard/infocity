# frozen_string_literal: true

require "rails_helper"

RSpec.describe Flow, type: :model do
  describe "#validate" do
    context "when nr_id is not present" do
      let!(:subject) { described_class.new(nr_id: nil) }

      it { is_expected.to be_invalid }
    end

    context "when name is not present" do
      let!(:subject) { described_class.new(name: nil) }

      it { is_expected.to be_invalid }
    end

    context "when alarm is not present" do
      let!(:subject) { described_class.new(alarm: nil) }

      it { is_expected.to be_invalid }
    end

    context "when nr_id, name and alarm are present" do
      let!(:alarm) { Alarm.create }
      let!(:subject) do
        described_class.new(nr_id: "my_id", name: true, alarm: alarm)
      end

      it { is_expected.to be_valid }
    end
  end

  describe "#build_matcher" do
    context "when it receives an Alarm " do
      let!(:alarm) { Alarm.create }

      before do
        allow_any_instance_of(Flow)
          .to receive(:create_nr_matcher)
          .and_return("flow_id")
      end

      it "builds a matcher flow" do
        expect(described_class.build_matcher(alarm, "matcher")).to be_a_kind_of(Flow)
      end
    end
  end

  describe ".destroy_nr_flow" do
    context "when there's no flow" do
      it "returns false" do
        expect(described_class.new.destroy_nr_flow).to be false
      end
    end
  end
end
