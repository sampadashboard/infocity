# frozen_string_literal: true

require "rails_helper"

RSpec.describe "events/new", type: :view do
  before(:each) do
    assign(:event, Event.new(
                     capability: "MyString",
                     comparator: "MyString",
                     value: "MyString",
                     alarm: Alarm.create
    ))
  end

  it "renders new event form" do
    render

    assert_select "form[action=?][method=?]", events_path, "post" do
      assert_select "input[name=?]", "event[capability]"

      assert_select "input[name=?]", "event[comparator]"

      assert_select "input[name=?]", "event[value]"

      assert_select "input[name=?]", "event[query_id]"
    end
  end
end
