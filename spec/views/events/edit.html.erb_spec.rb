# frozen_string_literal: true

require "rails_helper"

RSpec.describe "events/edit", type: :view do
  before(:each) do
    @event = assign(:event, Event.create!(
                              capability: "MyString",
                              comparator: "MyString",
                              value: "MyString",
                              alarm: Alarm.create
    ))
  end

  it "renders the edit event form" do
    render

    assert_select "form[action=?][method=?]", event_path(@event), "post" do
      assert_select "input[name=?]", "event[capability]"

      assert_select "input[name=?]", "event[comparator]"

      assert_select "input[name=?]", "event[value]"

      assert_select "input[name=?]", "event[query_id]"
    end
  end
end
