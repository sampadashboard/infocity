# frozen_string_literal: true

require "rails_helper"

RSpec.describe "events/index", type: :view do
  before(:each) do
    assign(:events, [
             Event.create!(
               capability: "Capability",
               comparator: "Comparator",
               value: "Value",
               alarm: Alarm.create
             ),
             Event.create!(
               capability: "Capability",
               comparator: "Comparator",
               value: "Value",
               alarm: Alarm.create
             )
           ])
  end

  it "renders a list of events" do
    render
    assert_select "tr>td", text: "Capability".to_s, count: 2
    assert_select "tr>td", text: "Comparator".to_s, count: 2
    assert_select "tr>td", text: "Value".to_s, count: 2
  end
end
