# frozen_string_literal: true

require "rails_helper"

RSpec.describe "events/show", type: :view do
  before(:each) do
    @event = assign(:event, Event.create!(
                              capability: "Capability",
                              comparator: "Comparator",
                              value: "Value",
                              alarm: Alarm.create
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Capability/)
    expect(rendered).to match(/Comparator/)
    expect(rendered).to match(/Value/)
    expect(rendered).to match(//)
  end
end
