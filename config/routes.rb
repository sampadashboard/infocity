# frozen_string_literal: true

Rails.application.routes.draw do
  resources :events
  resources :alarms

  resources :alarms do
    post :match, on: :member
  end

  get "welcome/index"
  root "welcome#index"

  get "/flows/master", to: "flow#new_master"
  get "/flows", to: "flow#new_matcher"
end
